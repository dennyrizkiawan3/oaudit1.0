<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/spktnp', 'SpktnpController@index'); // menampilkan halaman form
Route::get('/spktnp/create', 'SpktnpController@create'); // menampilkan halaman form
Route::post('/spktnp', 'SpktnpController@store'); // menyimpan data
Route::get('/spktnp/{id}', 'SpktnpController@show'); // menampilkan detail item dengan id 
Route::get('/spktnp/{id}/edit', 'SpktnpController@edit'); // menampilkan form untuk edit item
Route::put('/spktnp/{id}', 'SpktnpController@update'); // menyimpan perubahan dari form edit
Route::delete('/spktnp/{id}', 'SpktnpController@destroy'); // menghapus data dengan id
