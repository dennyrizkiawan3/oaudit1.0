<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SpktnpModel
{
    public static function getAll()
    {
        $spktnp = DB::table('spktnp')->get();
        return $spktnp;
    }

    public static function getById($id)
    {
        $spktnp = DB::table('spktnp')->where('id', '=', $id)->first();
        return $spktnp;
    }

    public static function save($spktnp)
    {
        $new_spktnp = DB::table('spktnp')->insert($spktnp);
        return 1;
    }

    public static function update($id, $request)
    {
        
        $spktnp = DB::table('spktnp')->where('id', $id)->update([
            'bea_masuk' => $request['bea_masuk'],
            'ppn' => $request['ppn'],
            'pph_pasal_22' => $request['pph_pasal_22'],
            'denda' => $request['denda'],
            'updated_at' => $request['updated_at']
        ]);
        return $spktnp;
    }

    public static function destroy($id)
    {
        $deleted = DB::table('spktnp')->where('id', $id)->delete();
        return $deleted;
    }
}
