<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpktnpModel;

class SpktnpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spktnp = SpktnpModel::getAll();
        return view('spktnp.index', compact('spktnp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('spktnp.addform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        
        
        $spktnp = SpktnpModel::save($data);
        if ($spktnp) {
            return redirect('/spktnp');
        } else {
            return view('spktnp.addform');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $spktnp = SpktnpModel::getById($id);
       
        return view('spktnp.show', compact('spktnp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $spktnp = SpktnpModel::getById($id);
        return view('spktnp.edit', compact('spktnp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $spktnp = SpktnpModel::update($id, $request->all());
        return redirect('/spktnp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = SpktnpModel::destroy($id);
        return redirect('/spktnp');
    }
    
}
