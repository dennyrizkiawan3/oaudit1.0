<?php
date_default_timezone_set("Asia/Jakarta");
$now = new DateTime();
?>
@extends('layouts/master')
@section('content')
<div class="row m-2">
    <h3>Edit SPKTNP</h3>
</div>
<div class="card m-2">
    <div class="card-body">
        <form action="{{url('/spktnp/'.$spktnp->id)}}" method="POST">
            @csrf
            @method('put')
            <input type="hidden" name="date_created" value="{{$spktnp->created_at}}">
            <input type="hidden" name="updated_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <div class="form-group">
                <label for="bea_masuk">Bea Masuk</label>
                <input type="integer" class="form-control" id="bea_masuk" placeholder="Bea Masuk" name="bea_masuk" value="{{$spktnp->bea_masuk}}">
            </div>
            <div class="form-group">
                <label for="ppn">PPN</label>
                <input type="integer" class="form-control" id="ppn" placeholder="PPN" name="ppn" value="{{$spktnp->ppn}}">
            </div>
            <div class="form-group">
                <label for="pph_pasal_22">PPh Pasal 22</label>
                <input type="integer" class="form-control" id="pph_pasal_22" placeholder="PPh Pasal 22" name="pph_pasal_22" value="{{$spktnp->pph_pasal_22}}">
            </div>
            <div class="form-group">
                <label for="denda">Denda</label>
                <input type="integer" class="form-control" id="denda" placeholder="Denda" name="denda" value="{{$spktnp->denda}}">
            </div>
            <button type="submit" class="btn btn-primary float-right">Update</button>
        </form>
    </div>
</div>
@endsection