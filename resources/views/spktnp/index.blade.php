@extends('layouts/master')
@section('content')
<div class="row">
    <h3>All SPKTNP</h3>
</div>
<div class="row">
	<a href="{{url('spktnp/create')}}" class="btn btn-primary float float-right">Buat SPKTNP</a>
</div>
<div class="row mt-2">
<!-- <div class="col"> -->
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Bea Masuk</th>
                <th scope="col">PPN</th>
                <th scope="col">PPh Pasal 22</th>
                <th scope="col">Denda</th>
                <th scope="col" style="display: inline">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($spktnp as $a)
            
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$a->bea_masuk}}</td>     
                <td>{{$a->ppn}}</td>
                <td>{{$a->pph_pasal_22}}</td>
                <td>{{$a->denda}}</td>
                <td class="text-center">
                	<a href="{{url('/spktnp/'.$a->id)}}" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i></a>
                    <a href="{{url('/spktnp/'.$a->id.'/edit')}}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                    <form action="/spktnp/{{$a->id}}" method="post" style="display: inline;">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
<!-- </div> -->
</div>
@endsection
