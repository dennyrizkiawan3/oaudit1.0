<!DOCTYPE html>
<html lang="en">

<style type="text/css">
            table {
                background: #fff;
                padding: 5px;
            }
            tr, td {
                border: table-cell;
                border: 1px  solid #444;
            }
             tr,td {
                vertical-align: top!important;
            }
            #right {
                border-right: none !important;
            }
            #left {
                border-left: none !important;
            }
            .isi {
                height: 300px!important;
            }
            .disp {
                text-align: center;
                padding: 1.5rem 0;
                margin-bottom: .5rem;
            }
            .logodisp {
                float: left;
                position: relative;
                width: 110px;
                height: 110px;
                margin: 0 0 0 1rem;
            }
            #lead {
                width: auto;
                position: relative;
                margin: 15px 0 0 65%;
            }
            .lead {
                font-weight: bold;
                text-decoration: underline;
                margin-bottom: -10px;
            }
            .tgh {
                text-align: center;
            }
            #nama {
                font-size: 17px;
                margin-bottom: -1rem;
            }
            #alamat {
                font-size: 10px;
            }
            .up {
                text-transform: uppercase;
                margin: 0;
                line-height: 2.2rem;
                font-size: 17px;
            }
            .status {
                margin: 0;
                font-size: 17px;
                font-weight: bold;
                margin-bottom: .5rem;
            }
            #lbr {
                font-size: 14px;
                font-weight: bold;
            }
            .separator {
                border-bottom: 2px solid #616161;
                margin: -1.3rem 0 1.5rem;
            }
</style>
        <body onload="window.print()">

        <!-- Container START -->
           <div style="font-weight: bold;"> KEMENTERIAN KEUANGAN REPUBLIK INDONESIA </div>
           <div style="font-weight: bold;" colspan="5"><u> DIREKTORAT JENDERAL BEA DAN CUKAI </u></div>
echo'
                    <table>
                        <tbody>
                            <tr>
                                <div style="height: 20px"></div>
                                <div style="text-align: center; font-weight: bold;" colspan="5"> <u>SURAT PENETAPAN KEMBALI TARIF DAN NILAI PABEAN (SPKTNP)</u></div>
                                <div style="text-align: center;" >Nomor: SPKTNP- {{$spktnp->id}}/BC/2022</div>
                                <div style="text-align: center;" >Tanggal: </div>
                                <div style="height: 10px;"></div>
                            </tr>
                            <tr>
                                <tr>
                                    <div>Kepada Yth.</div>
                                    <div>Nama   : </div>
                                    <div>NPWP   : </div>
                                    <div>Alamat : </div>
                                  


                            <div style="height: 40px;"></div>
                            </tr>

                            <tr>
                                <div style="text-indent: 40px;" align=justify> Berdasarkan Undang-Undang Nomor 10 tahun 1995 tentang Kepabeanan sebagaimana telah diubah dengan Undang-Undang Nomor 17 tahun 2006 pada Pasal 17 ayat (1), dan sesuai dengan .... nomor .... tanggal ...., ditetapkan kembali tarif dan/atau nilai pabean, sehingga mengakibatkan kekurangan dan/atau kelebihan pembayaran bea masuk dan/atau pajak dalam rangka impor serta sanksi administrasi berupa denda dengan rincian sebagai berikut:*) </div>
                            </tr>
                            <tr><td style="width: 30px" align=center>NO</td>
                                <td style="width: 200px" align=center>URAIAN</td>
                                <td style="width: 220px" align=center>KEKURANGAN </td>
                                <td style="width: 220px" align=center>KELEBIHAN </td></tr>
                            <tr><td align=center>1</td>
                                <td>Bea Masuk</td>
                                <td>Rp{{$spktnp->bea_masuk}}</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>2</td>
                                <td>BMAD/BMI/BMTP **)</td>
                                <td>Rp....</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>3</td>
                                <td>BMAS/BMIS/BMTPs **)</td>
                                <td>Rp....</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>4</td>
                                <td>Cukai</td>
                                <td>Rp....</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>5</td>
                                <td>PPN</td>
                                <td>Rp{{$spktnp->ppn}}</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>6</td>
                                <td>PPnBM</td>
                                <td>Rp....</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>7</td>
                                <td>PPh Pasal 22</td>
                                <td>Rp{{$spktnp->pph_pasal_22}}</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>8</td>
                                <td>Denda</td>
                                <td>Rp{{$spktnp->denda}}</td>
                                <td>Rp....</td></tr>
                            <tr><td align=center>9</td>
                                <td>....</td>
                                <td>Rp....</td>
                                <td>Rp....</td></tr>
                            <tr><td class="tgh" colspan="2" style="font-weight: bold;">Jumlah Tagihan</td>
                                <td style="font-weight: bold;">Rp....</td>
                                <td style="font-weight: bold;">Rp....</td></tr>
                                <div></tbody></table>

                            <div style="text-indent: 40px;" align=justify>
                            Dalam hal terdapat kekurangan pembayaran atas penetapan kembali tarif dan/atau nilai pabean, Saudara wajib melunasi kekurangan pembayaran tersebut paling lambat pada tanggal ..... dan bukti pelunasan disampaikan kepada Kepala ......</div>
                            <div style="text-indent: 40px;" align=justify>
                            Apabila tagihan tidak dilunasi sampai dengan tanggal 
                            ....., Saudara dikenakan bunga sebesar 2% (dua persen) setiap bulan dari jumlah kekurangan pembayaran bea masuk dan sanksi administrasi berupa denda untuk paling lama 24 (dua puluh empat) bulan, bagian bulan dihitung satu bulan penuh </div>
                            <div style="text-indent: 40px;" align=justify>
                            Dalam hal terdapat kelebihan pembayaran atas penetapan kembali tarif dan/atau nilai pabean, Saudara dapat mengajukan permohonan pengembalian sesuai ketentuan peraturan perundang-undangan. </div>
                            <div style="text-indent: 40px;" align=justify>
                            Apabila Saudara berkeberatan atas penetapan ini dapat mengajukan permohonan banding kepada Pengadilan Pajak paling lambat tanggal ......
                            </div>
                            </tr>

                <div id="lead">
                <div>a.n. Direktur Jenderal</div>
                <div>Direktur Audit Kepabeanan dan Cukai</div>

                <div style="height: 60px;"></div>';




                <div>Decy Arifinsjah</div>
                <div>NIP 19621214 199103 1 002</div>
                
            </div>
        </div>
        <div class="jarak2"></div>

        <tr><div align=justify colspan="5">
            SPKTNP ini dibuat rangkap 5 (lima): </div>
            <div> - Rangkap ke-1 untuk .....</div>
            <div> - Rangkap ke-2 untuk Direktur Jenderal</div>
            <div> - Rangkap ke-3 untuk Direktur Audit Kepabeanan dan Cukai</div>
            <div> - Rangkap ke-4 untuk Kepala .....</div>
            <div> - Rangkap ke-5 untuk Kepala Kantor Pengawasan dan Pelayanan .....</div>

                            
    <!-- Container END -->
    <div class="card-header">
            <a href="{{url('spktnp')}}" class="btn btn-sm btn-primary float-right"> Kembali </a>
        </div>

    </body>';
    
</html>