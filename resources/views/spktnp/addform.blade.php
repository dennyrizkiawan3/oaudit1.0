<?php
date_default_timezone_set("Asia/Jakarta");
$now = new DateTime();
?>
@extends('layouts.master')
@section('content')
<br><br>
<div class="row col-7">
    <h3>Tambah SPKTNP</h3>
</div>
<div class="row card">
    <div class="card-body">
        <form action="{{url('/spktnp/')}}" method="post">
            @csrf
            <input type="hidden" name="created_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <input type="hidden" name="updated_at" value="<?php echo $now->format('Y-m-d H:i:s'); ?>">
            <div class="form-group">
                <label for="bea_masuk">Bea Masuk</label>
                <input type="integer" class="form-control" id="bea_masuk" placeholder="Bea Masuk" name="bea_masuk">
            </div>
            <div class="form-group">
                <label for="ppn">PPN</label>
                <input type="integer" class="form-control" id="ppn" placeholder="PPN" name="ppn">
            </div>
            <div class="form-group">
                <label for="pph_pasal_22">PPh Pasal 22</label>
                <input type="integer" class="form-control" id="pph_pasal_22" placeholder="PPh Pasal 22" name="pph_pasal_22">
            </div>
            <div class="form-group">
                <label for="denda">Denda</label>
                <input type="integer" class="form-control" id="denda" placeholder="Denda" name="denda">
            </div>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
</div>
@endsection